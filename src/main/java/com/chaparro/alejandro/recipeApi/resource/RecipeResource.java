package com.chaparro.alejandro.recipeApi.resource;

import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.RecipeStore;
import com.chaparro.alejandro.recipeApi.store.exception.DuplicateRecipeNameException;
import com.chaparro.alejandro.recipeApi.store.exception.IllegalFieldValueException;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeNotFoundException;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

@Path("recipes")
@RequestScoped
public class RecipeResource {

    private RecipeStore recipeStore;

    @Inject
    public void setRecipeStore(RecipeStore recipeStore) {
        this.recipeStore = recipeStore;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRecipes(@QueryParam("q") @DefaultValue("") String criteria){

        if(criteria.isBlank()){
            return Response.ok(recipeStore.getRecipes()).build();
        }else{
            return Response.ok(recipeStore.searchRecipes(criteria)).build();
        }
    }

    @GET
    @Path("/{recId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRecipe(@PathParam("recId") String recipeId){

        try {
            return Response.ok(this.recipeStore.getRecipe(recipeId)).build();
        } catch (RecipeNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createRecipe(Recipe recipe){

        try {
            recipeStore.saveRecipe(recipe);
        } catch (RecipeStoreException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage())
                    .build();
        } catch (DuplicateRecipeNameException e) {
            return Response.status(Response.Status.CONFLICT).build();
        } catch (IllegalFieldValueException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE.getStatusCode(), e.getMessage()).build();
        }

        URI location = UriBuilder.fromResource(RecipeResource.class)
                .path("{recId}")
                .resolveTemplate("recId", recipe.getId())
                .build();

        return Response.created(location).build();
    }
}
