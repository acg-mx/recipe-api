package com.chaparro.alejandro.recipeApi.resource;

import com.chaparro.alejandro.recipeApi.model.Category;
import com.chaparro.alejandro.recipeApi.store.CategoryStore;
import com.chaparro.alejandro.recipeApi.store.exception.CategoryNotFoundException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

@Path("categories")
@RequestScoped
public class CategoryResource {

    private CategoryStore categoryStore;

    @Inject
    public void setCategoryStore(CategoryStore _categoryStore) {
        this.categoryStore = _categoryStore;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategories(){
        return Response.ok(categoryStore.getCategories()).build();
    }

    @GET
    @Path("/{catId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategory(@PathParam("catId") String categoryId){

        try {
            return Response.ok(this.categoryStore.getCategory(categoryId)).build();
        } catch (CategoryNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/{catId}/recipes")
    public Response getRecipesInCategory(@PathParam("catId") String categoryId){

        try {
            return Response.ok(
                    categoryStore.getCategory(categoryId)
                    .getRecipes()
            ).build();
        } catch (CategoryNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
