package com.chaparro.alejandro.recipeApi.application;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class RecipesBookAppConfiguration extends Application {
}
