package com.chaparro.alejandro.recipeApi.store.util;

import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Default
@ApplicationScoped
public class Sha256IdFactory implements IdFactory {

    private MessageDigest digest;

    public Sha256IdFactory() throws RecipeStoreException{
        try {
            digest = MessageDigest.getInstance("SHA-256");
        }catch (NoSuchAlgorithmException ex){
            throw new RecipeStoreException(String.format("Error generating recipes' ids: %s", ex.getMessage()));
        }
    }

    @Override
    public String createIdFromString(String input) {
        return Base64.getUrlEncoder().encodeToString(digest.digest(
                input.getBytes(StandardCharsets.UTF_8)));
    }
}
