package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Optional;

@XmlType(name = "ing-div")
public class XmlIngredientDivision {

    @XmlElement(name = "title")
    private String title;

    @XmlElement(name = "ing")
    private List<XmlIngredient> ingredients;

    public String getTitle() {
        return title;
    }

    public Optional<List<XmlIngredient>> getIngredients() {
        if(ingredients != null){
            return Optional.of(ingredients);
        }
        return Optional.empty();
    }
}
