package com.chaparro.alejandro.recipeApi.store.source;

import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;

import java.util.List;

/**
 * A DataSource is a place where Recipes can be loaded from.
 * It is intended to provide a DataStore with the base list
 * of Recipes it can operate on.
 */
public interface DataSource {

    /**
     * Loads a list of Recipes from a storage source such as files in
     * a directory, a database or a web access point.
     * @return List of recipes
     * @throws RecipeStoreException If there is an error loading or parsing the recipes.
     */
    List<Recipe> loadRecipes() throws RecipeStoreException;
}
