package com.chaparro.alejandro.recipeApi.store.exception;

public class IllegalFieldValueException extends Exception {

    public IllegalFieldValueException(String field, String value){
        super(String.format("Illegal field value.  Field: %s.  Value: %s", field, value));
    }

}
