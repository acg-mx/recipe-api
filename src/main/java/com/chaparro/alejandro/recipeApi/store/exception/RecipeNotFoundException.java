package com.chaparro.alejandro.recipeApi.store.exception;

public class RecipeNotFoundException extends Exception {

    private String recipeId;

    public RecipeNotFoundException(String _recipeId){
        this.recipeId = _recipeId;
    }

    public String getRecipeId() {
        return recipeId;
    }
}
