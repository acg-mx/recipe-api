package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.Optional;

@XmlType(name = "ingredients")
public class XmlIngredients {

    @XmlElement(name = "ing")
    private List<XmlIngredient> ingredients;

    @XmlElement(name = "ing-div")
    private List<XmlIngredientDivision> divisions;

    public Optional<List<XmlIngredient>> getIngredients() {
        if(ingredients != null){
            return Optional.of(ingredients);
        }
        return Optional.empty();
    }

    public Optional<List<XmlIngredientDivision>> getIngredientDivisions(){
        if(divisions != null){
            return Optional.of(divisions);
        }
        return Optional.empty();
    }
}
