package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ing")
public class XmlIngredient {

    @XmlElement(name = "item")
    private String name;

    @XmlElement(name = "amt")
    private XmlAmount amount;

    public String getName() {
        return name;
    }

    public XmlAmount getAmount() {
        return amount;
    }
}
