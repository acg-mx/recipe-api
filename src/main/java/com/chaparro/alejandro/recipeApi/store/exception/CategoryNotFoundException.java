package com.chaparro.alejandro.recipeApi.store.exception;

public class CategoryNotFoundException extends Exception {

    private String categoryId;

    public CategoryNotFoundException(String _categoryId){
        this.categoryId = _categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }
}
