package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "recipe")
public class XmlRecipe {

    @XmlElement(name = "head")
    private XmlHead head;

    @XmlElement(name = "ingredients")
    private XmlIngredients ingredients;

    @XmlElement(name = "directions")
    private XmlDirections directions;

    public XmlHead getHead() {
        return head;
    }

    public XmlIngredients getIngredients() {
        return ingredients;
    }

    public XmlDirections getDirections() {
        return directions;
    }
}
