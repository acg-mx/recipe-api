package com.chaparro.alejandro.recipeApi.store.util;

public interface IdFactory {

    String createIdFromString(String input);

}
