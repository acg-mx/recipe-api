package com.chaparro.alejandro.recipeApi.store.exception;

public class RecipeStoreException extends Exception {

    public RecipeStoreException(String reason){
        super(reason);
    }
}
