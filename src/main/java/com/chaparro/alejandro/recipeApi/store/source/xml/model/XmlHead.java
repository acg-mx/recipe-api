package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "head")
public class XmlHead {

    @XmlElement(name = "title")
    private String title;

    @XmlElement(name = "categories")
    private XmlCategories categories;

    @XmlElement(name = "yield")
    private int yield;

    public String getTitle() {
        return title;
    }

    public XmlCategories getCategories() {
        return categories;
    }

    public int getYield() {
        return yield;
    }
}
