package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "directions")
public class XmlDirections {

    @XmlElement(name = "step")
    private List<String> steps;

    public List<String> getSteps() {
        return steps;
    }
}
