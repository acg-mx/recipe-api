package com.chaparro.alejandro.recipeApi.store;

import com.chaparro.alejandro.recipeApi.model.Category;
import com.chaparro.alejandro.recipeApi.store.exception.CategoryNotFoundException;

import java.util.Collection;

public interface CategoryStore {

    Collection<Category> getCategories();

    Category getCategory(String id) throws CategoryNotFoundException;

}
