package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(name = "categories")
public class XmlCategories {

    @XmlElement(name = "cat")
    List<String> categories;

    public List<String> getCategories() {
        return categories;
    }
}
