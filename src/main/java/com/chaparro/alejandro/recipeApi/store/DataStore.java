package com.chaparro.alejandro.recipeApi.store;

import com.chaparro.alejandro.recipeApi.model.Category;
import com.chaparro.alejandro.recipeApi.model.Ingredient;
import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.exception.*;
import com.chaparro.alejandro.recipeApi.store.source.DataSource;
import com.chaparro.alejandro.recipeApi.store.util.IdFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

/**
 * A DataStore keeps a list of recipes in memory
 * and is capable of performing different operations
 * on them, such as locating a recipe by Id, searching
 * recipes by a search criteria, or adding new recipes
 * to the list.
 */
@ApplicationScoped
public class DataStore implements RecipeStore, CategoryStore {

    private DataSource dataSource;

    private IdFactory idFactory;

    private Map<String, Recipe> recipes = new HashMap<>();

    private Map<String, Category> categories = new HashMap<>();


    @Inject
    public DataStore(DataSource _dataSource, IdFactory _idFactory) throws RecipeStoreException, IllegalFieldValueException {

        this.dataSource = _dataSource;
        this.idFactory = _idFactory;

        for(Recipe recipe: this.dataSource.loadRecipes()){
            try {
                addRecipe(recipe);
            } catch (DuplicateRecipeNameException e) {
                System.err.println(String.format("Duplicated recipe name: %s.  Recipe ignored", recipe.getTitle()));
            }
        }
    }

    @Override
    public Collection<Category> getCategories() {
        return Collections.unmodifiableCollection(categories.values());
    }

    @Override
    public Category getCategory(String id) throws CategoryNotFoundException {

        Category category = categories.get(id);

        if(category == null){
            throw new CategoryNotFoundException(id);
        }

        return category;
    }

    @Override
    public Collection<Recipe> getRecipes() {
        return Collections.unmodifiableCollection(recipes.values());
    }

    @Override
    public Recipe getRecipe(String id) throws RecipeNotFoundException {

        Recipe recipe = recipes.get(id);

        if(recipe == null){
            throw new RecipeNotFoundException(id);
        }

        return recipe;
    }

    @Override
    public String saveRecipe(Recipe recipe) throws RecipeStoreException, DuplicateRecipeNameException, IllegalFieldValueException {
        addRecipe(recipe);
        return recipe.getId();
    }

    @Override
    public Collection<Recipe> searchRecipes(String criteria) {

        List<Recipe> foundRecipes = new LinkedList<>();

        recipes:
        for(Recipe recipe: recipes.values()){

            //Try to match by recipe title
            if(recipe.getTitle().contains(criteria)){
                foundRecipes.add(recipe);
                continue;
            }

            //Try to match by category
            for(String category: recipe.getCategories()){
                if(category.contains(criteria)){
                    foundRecipes.add(recipe);
                    continue recipes;
                }
            }

            //Try to match by ingredient
            for(List<Ingredient> ingredients: recipe.getIngredients().values()){
                for(Ingredient ingredient: ingredients){
                    if(ingredient.getName().contains(criteria)){
                        foundRecipes.add(recipe);
                        continue recipes;
                    }
                }
            }
        }

        return foundRecipes;
    }

    private void addRecipe(Recipe recipe) throws RecipeStoreException, DuplicateRecipeNameException, IllegalFieldValueException{

        if(recipe.getTitle() == null || recipe.getTitle().isBlank()){
            throw new IllegalFieldValueException("title", recipe.getTitle());
        }

        String recipeId = idFactory.createIdFromString(recipe.getTitle());

        if(recipes.containsKey(recipeId)){
            throw new DuplicateRecipeNameException(recipe.getTitle());
        }

        recipe.setId(recipeId);
        recipes.put(recipe.getId(), recipe);

        for(String category: recipe.getCategories()){

            String categoryId = idFactory.createIdFromString(category);

            if(!categories.containsKey(categoryId)){
                categories.put(categoryId, new Category(categoryId, category));
            }
            categories.get(categoryId).addRecipe(recipe);
        }
    }
}
