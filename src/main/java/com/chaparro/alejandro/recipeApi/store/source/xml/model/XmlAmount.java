package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "amt")
public class XmlAmount {

    @XmlElement(name = "qty")
    private String quantity;

    @XmlElement(name = "unit")
    private String unit;

    public String getQuantity() {
        return quantity;
    }

    public String getUnit() {
        return unit;
    }
}
