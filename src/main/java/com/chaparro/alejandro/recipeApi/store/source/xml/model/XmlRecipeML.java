package com.chaparro.alejandro.recipeApi.store.source.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "recipeml")
public class XmlRecipeML {

    @XmlElement(name = "recipe")
    private XmlRecipe recipe;

    public XmlRecipe getRecipe() {
        return recipe;
    }
}
