package com.chaparro.alejandro.recipeApi.store.exception;

public class DuplicateRecipeNameException extends Exception {

    private String recipeName;

    public DuplicateRecipeNameException(String _recipeName){
        this.recipeName = _recipeName;
    }

    public String getRecipeName() {
        return recipeName;
    }
}
