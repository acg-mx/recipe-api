package com.chaparro.alejandro.recipeApi.store;

import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.exception.DuplicateRecipeNameException;
import com.chaparro.alejandro.recipeApi.store.exception.IllegalFieldValueException;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeNotFoundException;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;

import java.util.Collection;

public interface RecipeStore {

    Collection<Recipe> getRecipes();

    Recipe getRecipe(String id) throws RecipeNotFoundException;

    String saveRecipe(Recipe recipe) throws RecipeStoreException, DuplicateRecipeNameException, IllegalFieldValueException;

    Collection<Recipe> searchRecipes(String criteria);

}
