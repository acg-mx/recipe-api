package com.chaparro.alejandro.recipeApi.store.source.xml;

import com.chaparro.alejandro.recipeApi.model.Direction;
import com.chaparro.alejandro.recipeApi.model.Ingredient;
import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;
import com.chaparro.alejandro.recipeApi.store.source.DataSource;
import com.chaparro.alejandro.recipeApi.store.source.xml.model.XmlIngredient;
import com.chaparro.alejandro.recipeApi.store.source.xml.model.XmlIngredientDivision;
import com.chaparro.alejandro.recipeApi.store.source.xml.model.XmlRecipe;
import com.chaparro.alejandro.recipeApi.store.source.xml.model.XmlRecipeML;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

@Default
@ApplicationScoped
public class XMLSource implements DataSource {

    private String dataDirectory;

    public XMLSource() throws RecipeStoreException {
        this("data");
    }

    public XMLSource(String _dataDirectory) throws RecipeStoreException {
        this.dataDirectory = _dataDirectory;
    }

    @Override
    public List<Recipe> loadRecipes() throws RecipeStoreException{

        Unmarshaller unmarshaller = getUnmarshaller();
        URL url = getClass().getClassLoader().getResource(dataDirectory);
        List<XmlRecipe> xmlRecipes = new LinkedList<>();
        List<Recipe> recipes = new LinkedList<>();

        if(url == null){
            throw new RecipeStoreException("Data source has not been found");
        }

        for(File recipeFile: new File(url.getPath()).listFiles()){

            String fileName = String.format("%s/%s", dataDirectory, recipeFile.getName());

            try(InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName)){
                xmlRecipes.add(((XmlRecipeML)unmarshaller.unmarshal(stream)).getRecipe());
            } catch (JAXBException | IOException ex) {
                System.err.println(
                        String.format(
                                "Error loading recipe from file: %s.  Error: %s",
                                recipeFile.getName(),
                                ex.getMessage()
                        )
                );
            }
        }

        for(XmlRecipe xmlRecipe: xmlRecipes){
            try {
                recipes.add(parseXmlRecipe(xmlRecipe));
            }catch(RecipeStoreException ex){
                System.err.println(
                        String.format(
                                "Error parsing recipe: %s.  Error: %s",
                                xmlRecipe.getHead().getTitle(),
                                ex.getMessage()
                        )
                );
            }
        }

        return recipes;
    }

    private Recipe parseXmlRecipe(XmlRecipe xmlRecipe) throws RecipeStoreException{

        Recipe recipe = new Recipe(
                xmlRecipe.getHead().getTitle(),
                xmlRecipe.getHead().getYield());

        for (String cat: xmlRecipe.getHead().getCategories().getCategories()){
            recipe.addCategory(cat);
        }

        if(xmlRecipe.getIngredients().getIngredients().isPresent()) {
            addIngredientsToRecipe(
                    recipe,
                    null,
                    xmlRecipe.getIngredients().getIngredients().get()
            );
        }

        if(xmlRecipe.getIngredients().getIngredientDivisions().isPresent()){
            for(XmlIngredientDivision division: xmlRecipe.getIngredients().getIngredientDivisions().get()){
                if(division.getIngredients().isPresent()) {
                    addIngredientsToRecipe(
                            recipe,
                            division.getTitle(),
                            division.getIngredients().get()
                    );
                }
            }
        }

        int directionCount = 1;
        for(String direction: xmlRecipe.getDirections().getSteps()){
            recipe.addDirection(new Direction(directionCount++, direction));
        }

        return recipe;
    }

    private void addIngredientsToRecipe(Recipe recipe, String group, List<XmlIngredient> ingredients) throws RecipeStoreException{
        for (XmlIngredient ing : ingredients) {
            recipe.addIngredient(
                    group,
                    new Ingredient(
                            ing.getName(),
                            ing.getAmount().getQuantity(),
                            ing.getAmount().getUnit()
                    )
            );
        }
    }

    private Unmarshaller getUnmarshaller() throws RecipeStoreException {
        try {
            return JAXBContext.newInstance(XmlRecipeML.class)
                    .createUnmarshaller();
        }catch(JAXBException ex){
            throw new RecipeStoreException(String.format("Error setting up JAXB unmarshaller: %s",
                    ex.getMessage()));
        }
    }
}
