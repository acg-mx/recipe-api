package com.chaparro.alejandro.recipeApi.model;

import java.util.Optional;

public class Ingredient {

    private String name;

    private String quantity;

    private String unit;

    public Ingredient() {
    }

    public Ingredient(String _name) {
        this(_name, null, null);
    }

    public Ingredient(String _name, String _quantity, String _unit) {
        this.name = _name;
        this.quantity = _quantity;
        this.unit = _unit;
    }

    public String getName() {
        return name;
    }

    public Optional<String> getQuantity() {
        if(quantity != null && !quantity.isBlank()){
            return Optional.of(quantity);
        }
        return Optional.empty();
    }

    public Optional<String> getUnit() {
        if(unit != null && !unit.isBlank()){
            return Optional.of(unit);
        }
        return Optional.empty();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
