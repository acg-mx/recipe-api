package com.chaparro.alejandro.recipeApi.model;

public class Direction {

    private int index;

    private String description;

    public Direction() {
    }

    public Direction(int index, String description) {
        this.index = index;
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public String getDescription() {
        return description;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
