package com.chaparro.alejandro.recipeApi.model;

import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Category {

    private String id;

    private String name;

    private List<Recipe> recipes = new LinkedList<>();

    public Category() {
    }

    public Category(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addRecipe(Recipe recipe) throws RecipeStoreException {
        if(recipe == null){
            throw new RecipeStoreException("Null recipe not allowed");
        }
        this.recipes.add(recipe);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Recipe> getRecipes() {
        return Collections.unmodifiableList(recipes);
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }
}
