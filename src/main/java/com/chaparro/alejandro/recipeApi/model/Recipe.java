package com.chaparro.alejandro.recipeApi.model;

import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;

import java.util.*;

public class Recipe {

    private String id;

    private String title;

    private int yield;

    private Map<String, List<Ingredient>> ingredients = new HashMap<>();

    private List<Direction> directions = new LinkedList<>();

    private List<String> categories = new LinkedList<>();

    public Recipe() {
    }

    public Recipe(String title, int yield) {
        this.title = title;
        this.yield = yield;
    }

    public void addIngredient(String group, Ingredient ingredient) throws RecipeStoreException{

        if(ingredient == null){
            throw new RecipeStoreException("Null ingredient not allowed");
        }

        if(group == null || group.isBlank()){
            group = "";
        }

        if(!this.ingredients.containsKey(group)){
            this.ingredients.put(group, new LinkedList<>());
        }
        this.ingredients.get(group).add(ingredient);
    }

    public void addDirection(Direction direction) throws RecipeStoreException{
        if(direction == null){
            throw new RecipeStoreException("Null direction not allowed");
        }
        this.directions.add(direction);
    }

    public void addCategory(String category) throws RecipeStoreException{
        if(category == null || category.isBlank()){
            throw new RecipeStoreException("Blank category not allowed");
        }
        this.categories.add(category);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public int getYield() {
        return yield;
    }

    public Map<String, List<Ingredient>> getIngredients() {

        Map<String, List<Ingredient>> _ingredients = new HashMap<>();

        for(String ingredientGroup: ingredients.keySet()){
            _ingredients.put(ingredientGroup, Collections.unmodifiableList(ingredients.get(ingredientGroup)));
        }

        return _ingredients;
    }

    public List<Direction> getDirections() {
        return Collections.unmodifiableList(directions);
    }

    public List<String> getCategories() {
        return Collections.unmodifiableList(categories);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYield(int yield) {
        this.yield = yield;
    }

    public void setIngredients(Map<String, List<Ingredient>> ingredients) {
        this.ingredients = ingredients;
    }

    public void setDirections(List<Direction> directions) {
        this.directions = directions;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}
