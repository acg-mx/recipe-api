package com.chaparro.alejandro.recipeApi.store.source.xml;

import com.chaparro.alejandro.recipeApi.model.Direction;
import com.chaparro.alejandro.recipeApi.model.Ingredient;
import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.exception.RecipeStoreException;
import com.chaparro.alejandro.recipeApi.store.source.DataSource;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class XMLSourceTest {

    @Test
    public void loadRecipes_whenAnXmlRecipeIsPresentInTheDataFolder_itsCorrectlyLoadedAndParsedIntoARecipe(){

        DataSource source = null;
        List<Recipe> recipes = null;

        try {
            source = new XMLSource("xmlResourceData/singleData");
        }catch(RecipeStoreException ex){
            assertEquals(null, ex, String.format("Exception when creating XMLSource: %s", ex.getMessage()));
        }

        try {
            recipes = source.loadRecipes();
        } catch (RecipeStoreException ex) {
            assertEquals(null, ex, String.format("Unexpected exception: %s", ex.getMessage()));
        }

        assertNotNull(recipes, "List of recipes received from source is null");
        assertEquals(1, recipes.size(), "Unexpected number of recipes returned by data source");

        Recipe recipe = recipes.get(0);

        assertAll("Recipe Head structure",
            () -> assertEquals("Amaretto Cake", recipe.getTitle()),
            () -> assertEquals(1, recipe.getYield()),
            () -> assertNotNull(recipe.getCategories()),
            () -> assertEquals(3, recipe.getCategories().size()),
            () -> assertTrue(recipe.getCategories().contains("Liquor")),
            () -> assertTrue(recipe.getCategories().contains("Cakes")),
            () -> assertTrue(recipe.getCategories().contains("Cake mixes"))
        );

        assertAll("Ingredients structure",
            () -> assertNotNull(recipe.getIngredients()),
            () -> assertEquals(2, recipe.getIngredients().size()),
            () -> assertTrue(recipe.getIngredients().containsKey("")),
            () -> assertEquals(4, recipe.getIngredients().get("").size()),
            () -> assertTrue(recipe.getIngredients().containsKey("GLAZE")),
            () -> assertEquals(5, recipe.getIngredients().get("GLAZE").size())
        );

        assertIngredient("", "Vanilla instant pudding", "1", "package", recipe);
        assertIngredient("", "Eggs", "4", null, recipe);
        assertIngredient("", "Toasted Almonds; chopped", "1 1/2", "cups", recipe);

        assertIngredient("GLAZE", "Sugar", "1/2", "cups", recipe);
        assertIngredient("GLAZE", "Butter", "2", "tablespoons", recipe);

        assertAll("Directions",
                () -> assertNotNull(recipe.getDirections()),
                () -> assertEquals(2, recipe.getDirections().size()),
                () -> assertDirection(1, "Step 1", recipe),
                () -> assertDirection(2, "Step 2", recipe)
        );
    }

    private void assertDirection(int index, String directionDescription, Recipe recipe){
        for(Direction direction: recipe.getDirections()){
            if(direction.getIndex() == index){
                assertEquals(directionDescription, direction.getDescription());
                return;
            }
        }
        assertTrue(false, String.format("Direction not found: %s", directionDescription));
    }

    private void assertIngredient(String group, String name, String quantity, String unit, Recipe recipe){

        for(Ingredient ingredient: recipe.getIngredients().get(group)){
            if(ingredient.getName().equals(name)){
                if(quantity != null){
                    assertEquals(quantity, ingredient.getQuantity().get());
                }else{
                    assertTrue(ingredient.getQuantity().isEmpty());
                }
                if(unit != null){
                    assertEquals(unit, ingredient.getUnit().get());
                }else{
                    assertTrue(ingredient.getUnit().isEmpty());
                }
                return;
            }
        }

        assertTrue(false, String.format("Ingredient %s in group %s not found", name, group));
    }

}
