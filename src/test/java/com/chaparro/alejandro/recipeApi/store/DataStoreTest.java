package com.chaparro.alejandro.recipeApi.store;

import com.chaparro.alejandro.recipeApi.model.Category;
import com.chaparro.alejandro.recipeApi.model.Recipe;
import com.chaparro.alejandro.recipeApi.store.exception.*;
import com.chaparro.alejandro.recipeApi.store.source.DataSource;
import com.chaparro.alejandro.recipeApi.store.util.IdFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DataStoreTest {

    private DataStore dataStore;

    @BeforeEach
    public void testSetup(
            @Mock DataSource dataSource, @Mock IdFactory idFactory
    ){
        List<Recipe> recipes = getTestRecipes();

        try {
            Mockito.lenient().when(dataSource.loadRecipes()).thenReturn(recipes);
        } catch (RecipeStoreException ex) {
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        }

        Mockito.lenient().when(idFactory.createIdFromString("Recipe 1")).thenReturn("RCP1");
        Mockito.lenient().when(idFactory.createIdFromString("Recipe 2")).thenReturn("RCP2");
        Mockito.lenient().when(idFactory.createIdFromString("New Recipe")).thenReturn("NRC");

        Mockito.lenient().when(idFactory.createIdFromString("Cat1")).thenReturn("CT1");
        Mockito.lenient().when(idFactory.createIdFromString("Cat2")).thenReturn("CT2");
        Mockito.lenient().when(idFactory.createIdFromString("Cat3")).thenReturn("CT3");
        Mockito.lenient().when(idFactory.createIdFromString("Cat4")).thenReturn("CT4");

        try {
            dataStore = new DataStore(dataSource, idFactory);
        } catch (RecipeStoreException ex) {
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        } catch (IllegalFieldValueException ex) {
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        }
    }

    private List<Recipe> getTestRecipes(){

        List<Recipe> recipes = new LinkedList<>();
        Recipe recipe1 = new Recipe("Recipe 1", 1);
        Recipe recipe2 = new Recipe("Recipe 2", 2);

        try {
            recipe1.addCategory("Cat1");
            recipe1.addCategory("Cat2");
            recipe1.addCategory("Cat3");
            recipe2.addCategory("Cat3");
            recipe2.addCategory("Cat4");
        }catch(RecipeStoreException ex){
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        }

        recipes.add(recipe1);
        recipes.add(recipe2);

        return recipes;
    }

    @Test
    public void getCategories_whenInvoked_itListsAllCategoriesFoundInRecipesProvidedByTheDataSource(){
        Collection<Category> categories = dataStore.getCategories();

        assertEquals(4, categories.size());
        categories.stream().forEach(
                category -> assertTrue(
                category.getName().equals("Cat1") ||
                        category.getName().equals("Cat2") ||
                        category.getName().equals("Cat3") ||
                        category.getName().equals("Cat4")
                )
        );
    }

    @Test
    public void getCategory_whenCategoryIdExists_theCorrespondingCategoryIsReturned(){
        try {
            final Category category = dataStore.getCategory("CT3");
            assertNotNull(category);
            assertAll("Category",
                    () -> assertEquals("CT3", category.getId()),
                    () -> assertEquals("Cat3", category.getName()),
                    () -> assertEquals(2, category.getRecipes().size())
            );

        } catch (CategoryNotFoundException ex) {
            assertTrue(false,
                    String.format("Unexpected exception: %s.  Category not found: %s", ex.getMessage(), ex.getCategoryId()));
        }
    }

    @Test
    public void getCategory_whenCategoryIdDoesntExist_CategoryNotFoundExceptionIsThrown(){

        assertThrows(CategoryNotFoundException.class, () ->
            dataStore.getCategory("UnknownId")
        );
    }

    @Test
    public void getRecipes_whenInvoked_itListsAllRecipesProvidedByTheDataSource(){

        Collection<Recipe> recipes = dataStore.getRecipes();

        assertEquals(2, recipes.size());
        recipes.stream().forEach(
                recipe -> assertTrue(
                        recipe.getTitle().equals("Recipe 1") ||
                                recipe.getTitle().equals("Recipe 2")
                )
        );
    }

    @Test
    public void getRecipe_whenRecipeIdExists_theCorrespondingRecipeIsReturned(){
        try {
            final Recipe recipe = dataStore.getRecipe("RCP1");
            assertNotNull(recipe);
            assertAll("Recipe",
                    () -> assertEquals("RCP1", recipe.getId()),
                    () -> assertEquals("Recipe 1", recipe.getTitle())
            );

        } catch (RecipeNotFoundException ex) {
            assertTrue(false,
                    String.format("Unexpected exception: %s.  Recipe not found: %s", ex.getMessage(), ex.getRecipeId()));
        }
    }

    @Test
    public void getRecipe_whenRecipeIdDoesntExist_RecipeNotFoundExceptionIsThrown(){

        assertThrows(RecipeNotFoundException.class, () ->
                dataStore.getRecipe("UnknownId")
        );
    }

    @Test
    public void saveRecipe_whenNoRecipeExistsWithTheSameName_aNewRecipeIsSuccessfullyCreated(){

        Recipe recipe = new Recipe("New Recipe", 1);

        try {
            dataStore.saveRecipe(recipe);
        } catch (RecipeStoreException ex) {
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        } catch (DuplicateRecipeNameException ex) {
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        } catch (IllegalFieldValueException ex) {
            assertTrue(false, String.format("Unexpected exception: %s", ex.getMessage()));
        }

        assertNotNull(recipe.getId());

        Recipe savedRecipe = null;

        try {
            savedRecipe = dataStore.getRecipe(recipe.getId());
        } catch (RecipeNotFoundException ex) {
            assertTrue(false, String.format("Recipe not found after creation: %s", ex.getMessage()));
        }

        assertEquals(recipe.getTitle(), savedRecipe.getTitle());
    }

    @Test
    public void saveRecipe_whenThereIsAlreadyARecipeWithTheSameName_DuplicateRecipeNameExceptionIsThrown(){

        Recipe recipe = new Recipe("Recipe 1", 1);

        assertThrows(DuplicateRecipeNameException.class, () ->
            dataStore.saveRecipe(recipe)
        );
    }

    @Test
    public void searchRecipes_whenThereIsAMatchByTitle_theFoundRecipesAreReturned(){

        Collection<Recipe> searchResults = dataStore.searchRecipes("Recipe");

        assertEquals(2, searchResults.size());

        for(Recipe recipe: searchResults){
            assertTrue(recipe.getTitle().contains("Recipe"));
        }
    }

    @Test
    public void searchRecipes_whenThereIsAMatchByCategory_theFoundRecipesAreReturned(){

        Collection<Recipe> searchResults = dataStore.searchRecipes("Cat3");

        assertEquals(2, searchResults.size());

        for(Recipe recipe: searchResults){
            assertTrue(recipe.getCategories().contains("Cat3"));
        }
    }
}
