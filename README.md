# Recipe Book API #

The Recipe Book API provides a set of endpoints that serve information about cook recipes.

## API Resouces ##

The Recipe Book API exposes two Rest Resources:

* /recipes
* /categories

### /recipes ###

This resource provides methods to retrieve, search and create recipes.  The next methods can be found under this endpoint:

#### GET /recipes?q=criteria

Retrieves a list of recipes in **JSON** format.  The query param 'q' is the search criteria.  It is optional and is used to narrow the list of results returned by the service.  If present, only those recipes that contain the search criteria within their name, ingredients or categories are returned.  Otherwise, all recipes are listed.

#### GET /recipes/{recId}/

Returns a specific recipe in **JSON** format by its unique identifier.  If no recipe with the provided identifier is found, the method returns a *Http 404 - Not found* response code.

#### POST /recipes

Receives a recipe in **JSON** format and stores it.  The method can produce any of the next response:

|Http code  |Response body          |Description                                            |
|-----------|:----------------------|:------------------------------------------------------|
|201        |URI of the new Recipe  |The recipe has been successfully created               |
|409        |-- none --             |Name of the recipe already exists                      |
|406        |-- none --             |Required field is missing or it contains a wrong value |
|500        |-- none --             |An unknown error happened while creating the recipe    |

### /categories ###

Categories group multiple recipes that share some characteristics.  This endpoint allows listing all available categories and the recipes within them.

#### GET /categories ####

Lists all available categories in **JSON** format.

#### GET /categories/{catId}/ ####

Retrieves a recipe category in **JSON** format by its unique identifier.  If no category is found with the provided identifier, the method returns a *Http 404 - Not found* response code. 

#### GET /categories/{catId}/recipes/ ####

Returns the list of recipes under a given category.  If no category is found with the provided identifier, the method returns a *Http 404 - Not found* response code.  Categories are created when 
recipes are added to the system.  Therefore, every category must have at least one recipe associated to it.

## Build and Deployment ##

This project can be built using Maven 3.6.0+ and can be deployed in a Docker container running Payara Server by using the provided Dockerfile file.

### Project Build ###

Once the project has been cloned, it is possible to build it by running the next Maven command from the project's root directory:

```
mvn package
```

Please notice that the command runs all unit tests within the project and succeeds only if they all pass.  After building the project, a *target* folder is created that contains a *recipe-api.war*.  To run the project, this file can be deployed in a Java web server.

### Project Deployment in Payara Server container ###

This project includes a Dockerfile file that can be used to create a Docker image with the *recipe-api.war* file deployed within a Payara Server instance.  After building the project, the next Docker command can be executed to create the Docker image:

```
docker build . -t kitchen-book:1.0
```
After the image has been built, the container can be run with the next command:

```
docker run -it -p 8080:8080 kitchen-book:1.0
```

After the Payara Server has been initiated, it is possible to access the endpoints at ``http://{ip}:8080/recipe-api/api/{endpoint_path}``


## Class Structure ##

The next diagram illustrates the most relevant components of the Class Structure that make up this project:

![Class structure](ClassDiagram.png)

There are five main components in this design:

* XML Unmarshalling model
* Data model
* Data source
* Data store
* Resources

### XML Unmarshalling model ###
The classes under *XML Unmarshalling model* are aimed to help with loading in memory the list of recipes from XML files.  These classes are annotated with JAXB annotations and their structure is bound to the structure of the XML files. 

### Data model ###
These classes represent the structure of Categories, Recipes, Ingredients, Directions and their relations.  This structure has been designed apart from the *XML Unmarshalling model* so that it is not dependent on the structure of the XML files.  Having the model decoupled from the XML structure in this way makes it easier to connect new data sources with different structures to the system without affecting any of the classes using the model.

### Data source ###
A Data Source is responsible for reading data from a source and mapping that data into *Data model* classes.  At this moment there is only one Data Source (the XML DataSource).  However, this design enables easily integrating new types of Data Sources if it were required in the future. 

### Data store ###

A Data Store is the administrator of the Data Model.  It loads the Data Model with help of the Data Source, and performs operations on it such as finding Recipes and adding new ones.

### Resources ###

The Resources are the endpoints the clients of the application connect to.  They are capable of receiving requests, performing the required operations with the help of either a RecipeStore or CategoryStore, and sending the right response back.

## TODO ##

Next items are proposed as the next steps to improve the project:

### Unit tests for REST Resource classes ###
So far unit tests have been implemented only for DataSource and DataStore classes.  This is good because those classes contain most of the business logic supported by the application.  However, RecipeResource and CategoryResource endpoints are not tested yet and they are responsible for the response bodies, response formats and response codes returned to clients of the API.  Therefore, it's important the logic within these classes is properly tested as well. 

### HATEOAS ###

At this moment when an entity is queried trhough any of the endpoints, it carries all the objects it's related to, together with all their attributes.  This is inefficient because a lot of data is transferred through the network with each request when actually the client consuming the service might not need all this information.  Therefore, in order to make an efficient usage of the network and to make the endpoints RESTful compliant, HATEOAS should be implemented in the response payloads so that instead of adding the entire related objects, only their URIs are added.  The clients of the endpoints can then use these links to navigate to those related objects. 

## Security ##

For now the endpoints are fully open.  No Authentication or Authorization is needed to access them.  Maybe no access validations are needed to access the GET methods, but it could be beneficial for the POST ones, so that not everybody is allowed to create new recipes, for example.
